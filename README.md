# Betting backend REST API

This is a simple REST API for a betting application

## Running it locally

Create a `.env` file in the root of the application based on the contents in the `.env.example` file.

Install dependencies with `npm i`. Spin up local postgres database with `docker compose up -d`. Run database migrations with `npx prisma migrate dev`

Run the application using `npm run dev`. The app is available at `localhost:8080`
