import { FastifyRequest, RawServerDefault, RouteGenericInterface } from 'fastify';
import { type AuthenticationService } from './authentication-service';
import { IncomingMessage } from 'http';
import { getTokenFromHeader } from './get-token-from-header';

export type AuthenticationMiddleware = ReturnType<typeof authenticationMiddlewareFactory>;

export const authenticationMiddlewareFactory = ({
  authenticationService,
}: {
  authenticationService: Pick<AuthenticationService, 'verifyAccessToken'>;
}) => {
  const authenticationMiddleware = (
    req: FastifyRequest<RouteGenericInterface, RawServerDefault, IncomingMessage, Record<string, unknown>>,
  ) => {
    const authHeader = req.headers.authorization;
    const token = getTokenFromHeader({ authHeader });
    const identity = authenticationService.verifyAccessToken({ token });
    req.identity = identity;
    return Promise.resolve();
  };
  return authenticationMiddleware;
};
