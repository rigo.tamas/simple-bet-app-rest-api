import { sign, verify } from 'jsonwebtoken';
import { ApiError } from '../errors/errors';

export type AuthenticationService = ReturnType<typeof authenticationServiceFactory>;
export type AccessTokenBody = { userId: number };
export type RefreshTokenBody = { userId: number; refreshTokenVersion: number };

export const authenticationServiceFactory = ({
  accessTokenSecret,
  refreshTokenSecret,
}: {
  accessTokenSecret: string;
  refreshTokenSecret: string;
}) => {
  const verifyToken = <T>({ token, secret }: { token: string; secret: string }) => {
    try {
      return verify(token, secret) as T;
    } catch (e) {
      throw new ApiError(401, 'Authentication error');
    }
  };

  const verifyAccessToken = ({ token }: { token: string }) => {
    return verifyToken<AccessTokenBody>({ token, secret: accessTokenSecret });
  };

  const verifyRefreshToken = ({ refreshToken }: { refreshToken: string }) => {
    return verifyToken<RefreshTokenBody>({ token: refreshToken, secret: refreshTokenSecret });
  };

  const createAccessToken = ({ userId }: { userId: number }) => {
    const accessTokenBody: AccessTokenBody = { userId };
    return sign(accessTokenBody, accessTokenSecret, { expiresIn: '5m' });
  };

  const createRefreshToken = ({
    userId,
    refreshTokenVersion,
    expiresImmediately,
  }: {
    userId: number;
    refreshTokenVersion: number;
    expiresImmediately?: boolean;
  }) => {
    const expiresIn = expiresImmediately ? '0s' : '7d';
    const refreshTokenBody: RefreshTokenBody = { userId, refreshTokenVersion };
    return sign(refreshTokenBody, refreshTokenSecret, { expiresIn: expiresIn });
  };

  return {
    verifyAccessToken,
    createRefreshToken,
    createAccessToken,
    verifyRefreshToken,
  };
};
