const tokenPrefix = 'Bearer ';

export const getTokenFromHeader = ({ authHeader }: { authHeader: string | undefined }) => {
  if (!authHeader) {
    return '';
  }
  if (!authHeader.startsWith(tokenPrefix)) {
    return '';
  }
  return authHeader.slice(tokenPrefix.length);
};
