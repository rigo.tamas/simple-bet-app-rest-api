import { type FastifyReply } from 'fastify';

export const setRefreshTokenCookie = ({ res, refreshToken }: { res: FastifyReply; refreshToken: string }) => {
  void res.setCookie('refresh', refreshToken, { httpOnly: true, sameSite: 'none', secure: true });
};
