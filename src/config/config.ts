/* eslint-disable n/no-process-env */
// process.env should only be accessed here. Afterwards, the env vars should only be accessed through the type safe config variable

import { parseEnv } from 'znv';
import * as dotenv from 'dotenv';
import { z } from 'zod';

export type Config = z.infer<typeof configSchema>;

const configSchemaRaw = {
  PORT: z.number().min(0).max(65535).describe('The port that the backend should listen on'),
  CORS_ALLOW_ORIGIN: z.string().min(1).describe('Cors allow origin header'),
  ACCESS_TOKEN_SECRET: z.string().min(1).describe('Access token secret'),
  REFRESH_TOKEN_SECRET: z.string().min(1).describe('Refresh token secret'),
  DATABASE_URL: z.string().min(1).describe('Connection string for the database'),
  LOG_LEVEL: z
    .enum(['emerg', 'alert', 'crit', 'error', 'warning', 'notice', 'info', 'debug'])
    .describe('The log level to be used by winston logger')
    .default('info'),
};

const configSchema = z.object(configSchemaRaw);

export const getConfig = (): Config => {
  dotenv.config();
  return parseEnv(process.env, configSchemaRaw);
};
