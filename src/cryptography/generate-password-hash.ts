import bcrypt from 'bcrypt';

export const generatePasswordHash = async ({ password }: { password: string }): Promise<string> => {
  const saltRounds = 10;
  return await bcrypt.hash(password, saltRounds);
};
