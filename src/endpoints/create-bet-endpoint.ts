import { z } from 'zod';
import { type FastifyZodServer } from '../fastify/fastify-server';
import { type AuthenticationMiddleware } from '../authentication/authentication-middleware';
import { ApiError } from '../errors/errors';
import { type CreateBetUseCase } from '../use-cases/create-bet-use-case';

export const createBetPayloadSchema = z.object({
  prize: z.number(),
  votedForSuccess: z.boolean(),
  challengeId: z.number(),
});

export const createBetResponseSchema = z.object({
  betId: z.number(),
});

export type CreateBetPayload = z.infer<typeof createBetPayloadSchema>;
export type CreateBetResponse = z.infer<typeof createBetResponseSchema>;

export const createBetEndpoint = ({
  fastifyServer,
  createBetUseCase,
  authenticationMiddleware,
}: {
  fastifyServer: FastifyZodServer;
  createBetUseCase: CreateBetUseCase;
  authenticationMiddleware: AuthenticationMiddleware;
}) => {
  fastifyServer.post(
    '/bet',
    {
      schema: {
        body: createBetPayloadSchema,
        response: {
          200: createBetResponseSchema,
        },
      },
      // this is not an issue, fastify also accepts promise responses for preValidation handler
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      preValidation: authenticationMiddleware,
    },
    async (req) => {
      if (!req.identity?.userId) {
        throw new ApiError(401, 'Unauthenticated');
      }
      return await createBetUseCase({ userId: req.identity.userId, createBetPayload: req.body });
    },
  );
};
