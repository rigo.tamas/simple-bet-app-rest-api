import { z } from 'zod';
import { type FastifyZodServer } from '../fastify/fastify-server';
import { type AuthenticationMiddleware } from '../authentication/authentication-middleware';
import { ApiError } from '../errors/errors';
import { type CreateChallengeUseCase } from '../use-cases/create-challenge-use-case';

export const createChallengePayloadSchema = z.object({
  description: z.string().min(1),
  endDate: z.coerce.date(),
  title: z.string().min(1),
});

export const createChallengeResponseSchema = z.object({
  challengeId: z.number(),
});

export type CreateChallengePayload = z.infer<typeof createChallengePayloadSchema>;
export type CreateChallengeResponse = z.infer<typeof createChallengeResponseSchema>;

export const createChallengeEndpoint = ({
  fastifyServer,
  createChallengeUseCase,
  authenticationMiddleware,
}: {
  fastifyServer: FastifyZodServer;
  createChallengeUseCase: CreateChallengeUseCase;
  authenticationMiddleware: AuthenticationMiddleware;
}) => {
  fastifyServer.post(
    '/challenge',
    {
      schema: {
        body: createChallengePayloadSchema,
        response: {
          200: createChallengeResponseSchema,
        },
      },
      // this is not an issue, fastify also accepts promise responses for preValidation handler
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      preValidation: authenticationMiddleware,
    },
    async (req) => {
      if (!req.identity?.userId) {
        throw new ApiError(401, 'Unauthenticated');
      }
      return await createChallengeUseCase({ userId: req.identity.userId, createChallengePayload: req.body });
    },
  );
};
