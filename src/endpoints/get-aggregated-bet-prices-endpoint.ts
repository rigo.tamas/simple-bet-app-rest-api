import { z } from 'zod';
import { type FastifyZodServer } from '../fastify/fastify-server';
import { type AuthenticationMiddleware } from '../authentication/authentication-middleware';
import { ApiError } from '../errors/errors';
import { type GetAggregatedBetUseCase } from '../use-cases/get-aggregated-bet-use-case';

export const getAggregatedBetResponseSchema = z.object({
  summedPrices: z.number().nullable(),
});

export type GetAggregatedBetResponse = z.infer<typeof getAggregatedBetResponseSchema>;

export const getAggregatedBetEndpoint = ({
  fastifyServer,
  getAggregatedBetUseCase,
  authenticationMiddleware,
}: {
  fastifyServer: FastifyZodServer;
  getAggregatedBetUseCase: GetAggregatedBetUseCase;
  authenticationMiddleware: AuthenticationMiddleware;
}) => {
  fastifyServer.get(
    '/bet/aggregated/:challengeId',
    {
      schema: {
        params: z.object({
          challengeId: z.string().transform((val) => parseInt(val)),
        }),
        response: {
          200: getAggregatedBetResponseSchema,
        },
      },
      // this is not an issue, fastify also accepts promise responses for preValidation handler
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      preValidation: authenticationMiddleware,
    },
    async (req) => {
      if (!req.identity?.userId) {
        throw new ApiError(401, 'Unauthenticated');
      }
      return await getAggregatedBetUseCase(req.params);
    },
  );
};
