import { z } from 'zod';
import { type FastifyZodServer } from '../fastify/fastify-server';
import { type AuthenticationMiddleware } from '../authentication/authentication-middleware';
import { ApiError } from '../errors/errors';
import { type GetChallengesUseCase } from '../use-cases/get-challenges-use-case';
import { challengeResultOptionalSchema } from '../schemas/challenge-schema';

export const getChallengesResponseSchema = z
  .object({
    id: z.number(),
    title: z.string(),
    description: z.string(),
    userId: z.number(),
    proofVideoLink: z.string().optional().nullable(),
    active: z.boolean(),
    endDate: z.coerce.date(),
    result: challengeResultOptionalSchema,
    Bets: z
      .object({
        id: z.number(),
        challengeId: z.number(),
        userId: z.number(),
        votedForSuccess: z.boolean(),
        prize: z.number(),
      })
      .array(),
  })
  .array();

export type GetChallengesResponse = z.infer<typeof getChallengesResponseSchema>;

export const getChallengesEndpoint = ({
  fastifyServer,
  getChallengesUseCase,
  authenticationMiddleware,
}: {
  fastifyServer: FastifyZodServer;
  getChallengesUseCase: GetChallengesUseCase;
  authenticationMiddleware: AuthenticationMiddleware;
}) => {
  fastifyServer.get(
    '/challenges',
    {
      schema: {
        response: {
          200: getChallengesResponseSchema,
        },
      },
      // this is not an issue, fastify also accepts promise responses for preValidation handler
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      preValidation: authenticationMiddleware,
    },
    async (req) => {
      if (!req.identity?.userId) {
        throw new ApiError(401, 'Unauthenticated');
      }
      return await getChallengesUseCase();
    },
  );
};
