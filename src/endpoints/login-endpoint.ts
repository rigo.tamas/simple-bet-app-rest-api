import { z } from 'zod';
import { type FastifyZodServer } from '../fastify/fastify-server';
import { type LoginUseCase } from '../use-cases/login-use-case';

export const loginPayloadSchema = z.object({
  email: z.string().min(1),
  password: z.string().min(1),
});

export const loginResponseSchema = z.object({
  accessToken: z.string(),
});

export type LoginPayload = z.infer<typeof loginPayloadSchema>;
export type LoginResponse = z.infer<typeof loginResponseSchema>;

export const loginEndpoint = ({
  fastifyServer,
  loginUseCase,
}: {
  fastifyServer: FastifyZodServer;
  loginUseCase: LoginUseCase;
}) => {
  fastifyServer.post(
    '/login',
    {
      schema: {
        body: loginPayloadSchema,
        response: {
          200: loginResponseSchema,
        },
      },
    },
    async (req, res) => {
      const { refreshToken, accessToken } = await loginUseCase({ loginPayload: req.body });
      void res.setCookie('refresh', refreshToken, { httpOnly: true, sameSite: 'none', secure: true });
      return { accessToken };
    },
  );
};
