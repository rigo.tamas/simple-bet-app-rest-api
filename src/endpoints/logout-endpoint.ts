import { z } from 'zod';
import { type AuthenticationMiddleware } from '../authentication/authentication-middleware';
import { type AuthenticationService } from '../authentication/authentication-service';
import { setRefreshTokenCookie } from '../authentication/set-refresh-token-cookie';
import { ApiError } from '../errors/errors';
import { type FastifyZodServer } from '../fastify/fastify-server';

export const logoutEndpoint = ({
  fastifyServer,
  authenticationService,
  authenticationMiddleware,
}: {
  fastifyServer: FastifyZodServer;
  authenticationService: AuthenticationService;
  authenticationMiddleware: AuthenticationMiddleware;
}) => {
  fastifyServer.post(
    '/logout',
    {
      schema: { 200: z.object({}) },
      preValidation: authenticationMiddleware,
    },
    (req, res) => {
      if (!req.identity?.userId) {
        throw new ApiError(401, 'Unauthenticated');
      }
      const newRefreshToken = authenticationService.createRefreshToken({
        refreshTokenVersion: -1,
        expiresImmediately: true,
        userId: req.identity.userId,
      });
      setRefreshTokenCookie({ res, refreshToken: newRefreshToken });
      return {};
    },
  );
};
