import { z } from 'zod';
import { type FastifyZodServer } from '../fastify/fastify-server';
import { type AuthenticationMiddleware } from '../authentication/authentication-middleware';
import { ApiError } from '../errors/errors';
import { type MarkChallengeDoneUseCase } from '../use-cases/mark-challenge-done-use-case';
import { challengeResultRequiredSchema } from '../schemas/challenge-schema';

export const markChallengeDonePayload = z.object({
  challengeId: z.number(),
  result: challengeResultRequiredSchema,
});

export const markChallengeDoneEndpoint = ({
  fastifyServer,
  markChallengeDoneUseCase,
  authenticationMiddleware,
}: {
  fastifyServer: FastifyZodServer;
  markChallengeDoneUseCase: MarkChallengeDoneUseCase;
  authenticationMiddleware: AuthenticationMiddleware;
}) => {
  fastifyServer.patch(
    '/challenge/done',
    {
      schema: {
        body: markChallengeDonePayload,
        response: {
          200: z.object({}),
        },
      },
      // this is not an issue, fastify also accepts promise responses for preValidation handler
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      preValidation: authenticationMiddleware,
    },
    async (req) => {
      if (!req.identity?.userId) {
        throw new ApiError(401, 'Unauthenticated');
      }
      return await markChallengeDoneUseCase(req.body);
    },
  );
};
