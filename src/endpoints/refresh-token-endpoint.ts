import { z } from 'zod';
import { type FastifyZodServer } from '../fastify/fastify-server';
import { ApiError } from '../errors/errors';
import { type RefreshTokenUseCase } from '../use-cases/refresh-token-use-case';
import { setRefreshTokenCookie } from '../authentication/set-refresh-token-cookie';

export const refreshTokenResponseSchema = z.object({
  accessToken: z.string(),
});

export const refreshTokenEndpoint = ({
  fastifyServer,
  refreshTokenUseCase,
}: {
  fastifyServer: FastifyZodServer;
  refreshTokenUseCase: RefreshTokenUseCase;
}) => {
  fastifyServer.post(
    '/refresh_token',
    {
      schema: {
        response: { 200: refreshTokenResponseSchema },
      },
    },
    async (req, res) => {
      const { refresh: refreshToken } = req.cookies;
      if (!refreshToken) {
        throw new ApiError(401, 'no refresh token found');
      }
      const { accessToken, newRefreshToken } = await refreshTokenUseCase({ refreshToken });
      setRefreshTokenCookie({ refreshToken: newRefreshToken, res });
      return { accessToken };
    },
  );
};
