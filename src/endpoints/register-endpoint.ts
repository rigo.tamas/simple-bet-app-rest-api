import { z } from 'zod';
import { type FastifyZodServer } from '../fastify/fastify-server';
import { type RegisterUseCase } from '../use-cases/register-use-case';
import { setRefreshTokenCookie } from '../authentication/set-refresh-token-cookie';

export const registerPayloadSchema = z.object({
  email: z.string().min(1),
  pictureUrl: z.string().min(1),
  firstName: z.string().min(1),
  lastName: z.string().min(1),
  password: z.string().min(1),
});

export const registerResponseSchema = z.object({
  accessToken: z.string(),
});

export type RegisterPayload = z.infer<typeof registerPayloadSchema>;
export type RegisterResponse = z.infer<typeof registerResponseSchema>;

export const registerEndpoint = ({
  fastifyServer,
  registerUseCase,
}: {
  fastifyServer: FastifyZodServer;
  registerUseCase: RegisterUseCase;
}) => {
  fastifyServer.post(
    '/register',
    {
      schema: {
        body: registerPayloadSchema,
        response: {
          201: registerResponseSchema,
        },
      },
    },
    async (req, res) => {
      const { refreshToken, accessToken } = await registerUseCase({ registerPayload: req.body });
      setRefreshTokenCookie({ res, refreshToken });
      return { accessToken };
    },
  );
};
