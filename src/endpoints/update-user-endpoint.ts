import { z } from 'zod';
import { type FastifyZodServer } from '../fastify/fastify-server';
import { type UpdateUserUseCase } from '../use-cases/update-user-use-case';
import { type AuthenticationMiddleware } from '../authentication/authentication-middleware';
import { ApiError } from '../errors/errors';

export const updateUserPayloadSchema = z
  .object({
    email: z.string().min(1).optional(),
    pictureUrl: z.string().min(1).optional(),
    firstName: z.string().min(1).optional(),
    lastName: z.string().min(1).optional(),
  })
  .strict();

export type UpdateUserPayload = z.infer<typeof updateUserPayloadSchema>;

export const updateUserEndpoint = ({
  fastifyServer,
  updateUserUseCase,
  authenticationMiddleware,
}: {
  fastifyServer: FastifyZodServer;
  updateUserUseCase: UpdateUserUseCase;
  authenticationMiddleware: AuthenticationMiddleware;
}) => {
  fastifyServer.patch(
    '/user',
    {
      schema: {
        body: updateUserPayloadSchema,
        response: {
          200: z.object({}),
        },
      },
      // this is not an issue, fastify also accepts promise responses for preValidation handler
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      preValidation: authenticationMiddleware,
    },
    async (req) => {
      if (!req.identity?.userId) {
        throw new ApiError(401, 'Unauthenticated');
      }
      await updateUserUseCase({ userId: req.identity.userId, updateUserInfo: req.body });
      return {};
    },
  );
};
