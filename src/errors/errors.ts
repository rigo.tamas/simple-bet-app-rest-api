export const statusText = (statusCode: number): string => {
  switch (statusCode) {
    case 400:
      return 'Bad Request';
    case 401:
      return 'Unauthenticated';
    case 403:
      return 'Unauthorized';
    case 404:
      return 'Not Found';
    case 500:
      return 'Internal Server Error';
    default:
      return 'Unknown Error';
  }
};

export const errorMapper = (err: unknown): { status: number; response: Record<string, unknown> } => {
  if (err instanceof ApiError) {
    return {
      status: err.status,
      response: {
        message: err.message,
        error: statusText(err.status),
        statusCode: err.status,
      },
    };
  }

  if (err instanceof Error) {
    throw err;
  }

  return {
    status: 500,
    response: {
      message: 'Unhandled error',
      error: statusText(500),
      statusCode: 500,
    },
  };
};

export class ApiError extends Error {
  constructor(
    public status: number,
    public message: string,
  ) {
    super(message);
  }
}
