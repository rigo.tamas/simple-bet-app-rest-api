import fastify, {
  FastifyBaseLogger,
  FastifyInstance,
  RawReplyDefaultExpression,
  RawRequestDefaultExpression,
  RawServerDefault,
} from 'fastify';
import cors from '@fastify/cors';
import cookie from '@fastify/cookie';
import { Config } from '../config/config';
import { jsonSchemaTransform, serializerCompiler, validatorCompiler, ZodTypeProvider } from 'fastify-type-provider-zod';
import { errorMapper } from '../errors/errors';
import fastifySwagger from '@fastify/swagger';
import fastifySwaggerUI from '@fastify/swagger-ui';

export type FastifyZodServer = FastifyInstance<
  RawServerDefault,
  RawRequestDefaultExpression<RawServerDefault>,
  RawReplyDefaultExpression<RawServerDefault>,
  FastifyBaseLogger,
  ZodTypeProvider
>;

declare module 'fastify' {
  interface FastifyRequest {
    identity?: {
      userId?: number;
    };
  }
}

export const fastifyServerFactory = async ({ config }: { config: Config }) => {
  const fastifyServer = fastify({ logger: true }).withTypeProvider<ZodTypeProvider>();
  await fastifyServer.register(cors, { origin: config.CORS_ALLOW_ORIGIN });
  await fastifyServer.register(cookie, {
    parseOptions: {},
  });
  // need to decorate request for performance reasons. Will do the actual decoration in the preValidation hook of the endpoints
  fastifyServer.decorateRequest('identity', undefined);
  fastifyServer.setErrorHandler(async (error, _request, reply) => {
    const { status, response } = errorMapper(error);
    reply.statusCode = status;
    await reply.send(response);
  });

  fastifyServer.setValidatorCompiler(validatorCompiler);
  fastifyServer.setSerializerCompiler(serializerCompiler);
  await fastifyServer.register(fastifySwagger, {
    openapi: {
      info: {
        title: 'SampleApi',
        description: 'Sample backend service',
        version: '1.0.0',
      },
      servers: [],
    },
    transform: jsonSchemaTransform,
  });
  await fastifyServer.register(fastifySwaggerUI, {
    routePrefix: '/documentation',
  });

  return fastifyServer;
};
