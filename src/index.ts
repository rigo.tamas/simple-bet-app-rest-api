import { getConfig } from './config/config';
import { fastifyServerFactory } from './fastify/fastify-server';
import { createLogger } from './logger/logger';
import { PrismaClient } from '@prisma/client';
import { userRepositoryFactory } from './repositories/user-repo';
import { registerUseCaseFactory } from './use-cases/register-use-case';
import { authenticationServiceFactory } from './authentication/authentication-service';
import { registerEndpoint } from './endpoints/register-endpoint';
import { loginEndpoint } from './endpoints/login-endpoint';
import { loginUseCaseFactory } from './use-cases/login-use-case';
import { updateUserEndpoint } from './endpoints/update-user-endpoint';
import { authenticationMiddlewareFactory } from './authentication/authentication-middleware';
import { updateUserUseCaseFactory } from './use-cases/update-user-use-case';
import { refreshTokenEndpoint } from './endpoints/refresh-token-endpoint';
import { refreshTokenUseCaseFactory } from './use-cases/refresh-token-use-case';
import { logoutEndpoint } from './endpoints/logout-endpoint';
import { challengeRepositoryFactory } from './repositories/challenge-repo';
import { createChallengeUseCaseFactory } from './use-cases/create-challenge-use-case';
import { createChallengeEndpoint } from './endpoints/create-challenge-endpoint';
import { getChallengesEndpoint } from './endpoints/get-challenges-endpoint';
import { getChallengesUseCaseFactory } from './use-cases/get-challenges-use-case';
import { markChallengeDoneEndpoint } from './endpoints/mark-challenge-done-endpoint';
import { markChallengeDoneUseCaseFactory } from './use-cases/mark-challenge-done-use-case';
import { createBetUseCaseFactory } from './use-cases/create-bet-use-case';
import { betRepositoryFactory } from './repositories/bet-repo';
import { createBetEndpoint } from './endpoints/create-bet-endpoint';
import { getAggregatedBetEndpoint } from './endpoints/get-aggregated-bet-prices-endpoint';
import { getAggregatedBetUseCaseFactory } from './use-cases/get-aggregated-bet-use-case';

const main = async () => {
  const config = getConfig();
  const prisma = new PrismaClient();
  const logger = createLogger({ logLevel: config.LOG_LEVEL });

  const fastifyServer = await fastifyServerFactory({ config });

  // services
  const authenticationService = authenticationServiceFactory({
    accessTokenSecret: config.ACCESS_TOKEN_SECRET,
    refreshTokenSecret: config.REFRESH_TOKEN_SECRET,
  });
  const authenticationMiddleware = authenticationMiddlewareFactory({ authenticationService });

  // repositories
  const userRepository = userRepositoryFactory({ prisma });
  const challengeRepository = challengeRepositoryFactory({ prisma });
  const betRepository = betRepositoryFactory({ prisma });

  // use cases
  const registerUseCase = registerUseCaseFactory({ userRepository, authenticationService });
  const loginUseCase = loginUseCaseFactory({ authenticationService, userRepository });
  const updateUserUseCase = updateUserUseCaseFactory({ userRepository });
  const refreshTokenUseCase = refreshTokenUseCaseFactory({ authenticationService, userRepository });
  const createChallengeUseCase = createChallengeUseCaseFactory({ challengeRepository });
  const getChallengesUseCase = getChallengesUseCaseFactory({ challengeRepository });
  const markChallengeDoneUseCase = markChallengeDoneUseCaseFactory({ challengeRepository });
  const createBetUseCase = createBetUseCaseFactory({ betRepository });
  const getAggregatedBetUseCase = getAggregatedBetUseCaseFactory({ betRepository });

  // endpoints
  registerEndpoint({ fastifyServer, registerUseCase });
  loginEndpoint({ fastifyServer, loginUseCase });
  updateUserEndpoint({ fastifyServer, authenticationMiddleware, updateUserUseCase });
  refreshTokenEndpoint({ fastifyServer, refreshTokenUseCase });
  logoutEndpoint({ fastifyServer, authenticationMiddleware, authenticationService });
  createChallengeEndpoint({ fastifyServer, authenticationMiddleware, createChallengeUseCase });
  getChallengesEndpoint({ fastifyServer, authenticationMiddleware, getChallengesUseCase });
  markChallengeDoneEndpoint({ fastifyServer, authenticationMiddleware, markChallengeDoneUseCase });
  createBetEndpoint({ fastifyServer, authenticationMiddleware, createBetUseCase });
  getAggregatedBetEndpoint({ fastifyServer, authenticationMiddleware, getAggregatedBetUseCase });

  await fastifyServer.listen({ port: config.PORT });
  fastifyServer.swagger();
  logger.info(`Server listening on port ${config.PORT}`);
};

void main();
