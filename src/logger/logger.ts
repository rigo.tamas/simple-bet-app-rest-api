import winston from 'winston';
import { Config } from '../config/config';

export const createLogger = ({ logLevel }: { logLevel: Config['LOG_LEVEL'] }) => {
  return winston.createLogger({
    level: logLevel,
    format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
    transports: [new winston.transports.Console()],
  });
};
