import { type PrismaClient, type User } from '@prisma/client';
import { type CreateBetPayload } from '../endpoints/create-bet-endpoint';

export type BetRepository = ReturnType<typeof betRepositoryFactory>;

export const betRepositoryFactory = ({ prisma }: { prisma: PrismaClient }) => {
  const createBet = async ({
    createBetPayload,
    userId,
  }: {
    createBetPayload: CreateBetPayload;
    userId: User['id'];
  }) => {
    const { id } = await prisma.bet.create({
      data: {
        prize: createBetPayload.prize,
        votedForSuccess: createBetPayload.votedForSuccess,
        challengeId: createBetPayload.challengeId,
        userId,
      },
    });
    return { betId: id };
  };

  const getAggregatedBets = async ({ challengeId }: { challengeId: number }) => {
    const aggregateQueryResult = await prisma.bet.aggregate({
      where: {
        challengeId,
      },
      _sum: { prize: true },
    });
    return aggregateQueryResult._sum;
  };

  return {
    createBet,
    getAggregatedBets,
  };
};
