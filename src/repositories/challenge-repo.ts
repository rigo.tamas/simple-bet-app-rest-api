import { type Challenge, type PrismaClient, type User } from '@prisma/client';
import { type CreateChallengePayload } from '../endpoints/create-challenge-endpoint';
import { type ChallengeResultRequiredLiterals } from '../schemas/challenge-schema';

export type ChallengeRepository = ReturnType<typeof challengeRepositoryFactory>;

export const challengeRepositoryFactory = ({ prisma }: { prisma: PrismaClient }) => {
  const createChallenge = async ({
    createChallengePayload,
    userId,
  }: {
    createChallengePayload: CreateChallengePayload;
    userId: User['id'];
  }) => {
    const { id } = await prisma.challenge.create({
      data: {
        description: createChallengePayload.description,
        title: createChallengePayload.title,
        endDate: createChallengePayload.endDate,
        userId,
      },
    });
    return { challengeId: id };
  };

  const getChallenges = async () => {
    return await prisma.challenge.findMany({
      include: {
        Bets: true,
      },
    });
  };

  const markChallengeAsDone = async ({
    challengeId,
    result,
  }: {
    challengeId: Challenge['id'];
    result: ChallengeResultRequiredLiterals;
  }) => {
    await prisma.challenge.update({
      where: { id: challengeId },
      data: { active: false, result },
    });
  };

  return {
    createChallenge,
    getChallenges,
    markChallengeAsDone,
  };
};
