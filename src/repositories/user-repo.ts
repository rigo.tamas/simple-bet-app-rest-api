import { User, PrismaClient } from '@prisma/client';
import { type RegisterPayload } from '../endpoints/register-endpoint';
import { generatePasswordHash } from '../cryptography/generate-password-hash';

export type UserRepository = ReturnType<typeof userRepositoryFactory>;

export const userRepositoryFactory = ({ prisma }: { prisma: PrismaClient }) => {
  const createUser = async ({ registerPayload }: { registerPayload: RegisterPayload }) => {
    const passwordHash = await generatePasswordHash({ password: registerPayload.password });
    const { id, refreshTokenVersion } = await prisma.user.create({
      data: {
        email: registerPayload.email,
        firstName: registerPayload.firstName,
        lastName: registerPayload.lastName,
        pictureUrl: registerPayload.pictureUrl,
        passwordHash,
      },
    });
    return { userId: id, refreshTokenVersion };
  };

  const getUserByEmail = async ({ email }: { email: string }) => {
    const user = await prisma.user.findUnique({ where: { email } });
    return user;
  };

  const getUserById = async ({ userId }: { userId: number }) => {
    const user = await prisma.user.findUnique({ where: { id: userId } });
    return user;
  };

  const updateUser = async (
    userData: Pick<User, 'id'> & Partial<Pick<User, 'firstName' | 'email' | 'lastName' | 'pictureUrl'>>,
  ) => {
    const { id, ...fieldsToUpdate } = userData;
    const user = await prisma.user.update({ where: { id }, data: fieldsToUpdate });
    return user;
  };

  return {
    createUser,
    getUserByEmail,
    updateUser,
    getUserById,
  };
};
