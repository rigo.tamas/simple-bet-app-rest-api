import { z } from 'zod';

export const challengeResultRequiredSchema = z.union([z.literal('Success'), z.literal('Failure')]);
export const challengeResultOptionalSchema = challengeResultRequiredSchema.optional().nullable();

export type ChallengeResultOptionalLiterals = z.infer<typeof challengeResultOptionalSchema>;
export type ChallengeResultRequiredLiterals = z.infer<typeof challengeResultRequiredSchema>;
