import { User } from '@prisma/client';
import { type CreateBetPayload } from '../endpoints/create-bet-endpoint';
import { type BetRepository } from '../repositories/bet-repo';

export type CreateBetUseCase = ReturnType<typeof createBetUseCaseFactory>;

export const createBetUseCaseFactory = ({ betRepository }: { betRepository: Pick<BetRepository, 'createBet'> }) => {
  const createBetUseCase = async ({
    userId,
    createBetPayload,
  }: {
    userId: User['id'];
    createBetPayload: CreateBetPayload;
  }) => {
    return await betRepository.createBet({
      createBetPayload,
      userId,
    });
  };

  return createBetUseCase;
};
