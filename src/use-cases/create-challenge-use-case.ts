import { User } from '@prisma/client';
import { type CreateChallengePayload } from '../endpoints/create-challenge-endpoint';
import { type ChallengeRepository } from '../repositories/challenge-repo';

export type CreateChallengeUseCase = ReturnType<typeof createChallengeUseCaseFactory>;

export const createChallengeUseCaseFactory = ({
  challengeRepository,
}: {
  challengeRepository: Pick<ChallengeRepository, 'createChallenge'>;
}) => {
  const createChallengeUseCase = async ({
    userId,
    createChallengePayload,
  }: {
    userId: User['id'];
    createChallengePayload: CreateChallengePayload;
  }) => {
    return await challengeRepository.createChallenge({
      createChallengePayload,
      userId,
    });
  };

  return createChallengeUseCase;
};
