import { type Challenge } from '@prisma/client';
import { type BetRepository } from '../repositories/bet-repo';

export type GetAggregatedBetUseCase = ReturnType<typeof getAggregatedBetUseCaseFactory>;

export const getAggregatedBetUseCaseFactory = ({
  betRepository,
}: {
  betRepository: Pick<BetRepository, 'getAggregatedBets'>;
}) => {
  const getAggregatedBetUseCase = async ({ challengeId }: { challengeId: Challenge['id'] }) => {
    const { prize: summedPrices } = await betRepository.getAggregatedBets({ challengeId });
    return { summedPrices };
  };
  return getAggregatedBetUseCase;
};
