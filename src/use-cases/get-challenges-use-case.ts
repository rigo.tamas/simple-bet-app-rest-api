import { type ChallengeRepository } from '../repositories/challenge-repo';

export type GetChallengesUseCase = ReturnType<typeof getChallengesUseCaseFactory>;

export const getChallengesUseCaseFactory = ({
  challengeRepository,
}: {
  challengeRepository: Pick<ChallengeRepository, 'getChallenges'>;
}) => {
  const getChallengesUseCase = async () => {
    return await challengeRepository.getChallenges();
  };
  return getChallengesUseCase;
};
