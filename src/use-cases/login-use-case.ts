import { compare } from 'bcrypt';
import { type AuthenticationService } from '../authentication/authentication-service';
import { ApiError } from '../errors/errors';
import { type UserRepository } from '../repositories/user-repo';
import { type LoginPayload } from '../endpoints/login-endpoint';

export type LoginUseCase = ReturnType<typeof loginUseCaseFactory>;

export const loginUseCaseFactory = ({
  userRepository,
  authenticationService,
}: {
  userRepository: Pick<UserRepository, 'getUserByEmail'>;
  authenticationService: Pick<AuthenticationService, 'createRefreshToken' | 'createAccessToken'>;
}) => {
  const loginUseCase = async ({ loginPayload: { email, password } }: { loginPayload: LoginPayload }) => {
    const user = await userRepository.getUserByEmail({ email });
    if (!user) {
      throw new ApiError(401, 'User not found');
    }

    const { id: userId, refreshTokenVersion, passwordHash: passwordHashFromDb } = user;
    const valid = await compare(password, passwordHashFromDb);
    if (!valid) {
      throw new ApiError(401, 'Wrong password');
    }

    const refreshToken = authenticationService.createRefreshToken({ userId, refreshTokenVersion });
    const accessToken = authenticationService.createAccessToken({ userId });
    return { refreshToken, accessToken };
  };
  return loginUseCase;
};
