import { type Challenge } from '@prisma/client';
import { type ChallengeRepository } from '../repositories/challenge-repo';
import { type ChallengeResultRequiredLiterals } from '../schemas/challenge-schema';

export type MarkChallengeDoneUseCase = ReturnType<typeof markChallengeDoneUseCaseFactory>;

export const markChallengeDoneUseCaseFactory = ({
  challengeRepository,
}: {
  challengeRepository: Pick<ChallengeRepository, 'markChallengeAsDone'>;
}) => {
  const markChallengeDoneUseCase = async ({
    challengeId,
    result,
  }: {
    challengeId: Challenge['id'];
    result: ChallengeResultRequiredLiterals;
  }) => {
    return await challengeRepository.markChallengeAsDone({ challengeId, result });
  };
  return markChallengeDoneUseCase;
};
