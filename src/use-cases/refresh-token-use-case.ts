import { type AuthenticationService } from '../authentication/authentication-service';
import { ApiError } from '../errors/errors';
import { type UserRepository } from '../repositories/user-repo';

export type RefreshTokenUseCase = ReturnType<typeof refreshTokenUseCaseFactory>;

export const refreshTokenUseCaseFactory = ({
  authenticationService,
  userRepository,
}: {
  authenticationService: Pick<AuthenticationService, 'createAccessToken' | 'createRefreshToken' | 'verifyRefreshToken'>;
  userRepository: Pick<UserRepository, 'getUserById'>;
}) => {
  const refreshTokenUseCase = async ({ refreshToken }: { refreshToken: string }) => {
    const { userId, refreshTokenVersion } = authenticationService.verifyRefreshToken({ refreshToken });
    const user = await userRepository.getUserById({ userId });
    if (!user) {
      throw new ApiError(404, 'User not found');
    }
    const { refreshTokenVersion: refreshTokenVersionFromDb } = user;
    if (refreshTokenVersionFromDb > refreshTokenVersion) {
      throw new ApiError(401, 'Refresh token outdated');
    }
    const accessToken = authenticationService.createAccessToken({ userId });
    const newRefreshToken = authenticationService.createRefreshToken({
      userId,
      refreshTokenVersion: refreshTokenVersionFromDb,
    });
    return { accessToken, newRefreshToken };
  };

  return refreshTokenUseCase;
};
