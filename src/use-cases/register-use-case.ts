import { type AuthenticationService } from '../authentication/authentication-service';
import { type RegisterPayload } from '../endpoints/register-endpoint';
import { type UserRepository } from '../repositories/user-repo';

export type RegisterUseCase = ReturnType<typeof registerUseCaseFactory>;

export const registerUseCaseFactory = ({
  authenticationService,
  userRepository,
}: {
  authenticationService: Pick<AuthenticationService, 'createRefreshToken' | 'createAccessToken'>;
  userRepository: Pick<UserRepository, 'createUser'>;
}) => {
  const registerUseCase = async ({ registerPayload }: { registerPayload: RegisterPayload }) => {
    const { userId, refreshTokenVersion } = await userRepository.createUser({ registerPayload });
    const refreshToken = authenticationService.createRefreshToken({ userId, refreshTokenVersion });
    const accessToken = authenticationService.createAccessToken({ userId });
    return { refreshToken, accessToken };
  };

  return registerUseCase;
};
