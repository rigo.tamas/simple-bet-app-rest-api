import { type UpdateUserPayload } from '../endpoints/update-user-endpoint';
import { type UserRepository } from '../repositories/user-repo';

export type UpdateUserUseCase = ReturnType<typeof updateUserUseCaseFactory>;

export const updateUserUseCaseFactory = ({
  userRepository,
}: {
  userRepository: Pick<UserRepository, 'updateUser'>;
}) => {
  const updateUserUseCase = async ({
    userId,
    updateUserInfo,
  }: {
    userId: number;
    updateUserInfo: UpdateUserPayload;
  }) => {
    await userRepository.updateUser({ id: userId, ...updateUserInfo });
  };
  return updateUserUseCase;
};
